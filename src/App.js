import React, { Fragment, useState, useEffect } from 'react';
import Form from './components/Form';
import Appointment from './components/Appointment';

function App() {

  let initAppointments = JSON.parse(localStorage.getItem('appointments'));
  console.log(initAppointments)
  if (!initAppointments) {
    initAppointments = []
  }

  // Apointments array
  const [apointments, saveAppointments] = useState(initAppointments);

  useEffect(() => {
    if (initAppointments) {
      localStorage.setItem('appointments', JSON.stringify(apointments))
    } else {
      localStorage.setItem('appointments', JSON.stringify([]))
    }
  }, [apointments])

  const createApointment = apointment => {
    console.log(apointment)
    saveAppointments([
      ...apointments,
      apointment
    ])
  }

  const deleteAppointment = apId => {
    console.log("deleting...");
    const newAppointments = apointments.filter(appointment => appointment.id !== apId) 
    saveAppointments(newAppointments);
  }

  const title = apointments.length === 0 ? 'No Appointments' : 'Your Appointments'

  return (
    <Fragment>
      <h1>Pacient Management</h1>
      <div className="container">
        <div className="row">
          <div className="one-half column">
            <Form
              createApointment={createApointment}
            />
          </div>
          <div className="one-half column">
            <h2>{ title }</h2>
            {apointments.map(apointment => (
              <Appointment
                key={apointment.id}
                apointment= {apointment}
                deleteAppointment= {deleteAppointment}
              />
            ))}
          </div>
        </div>
      </div>
    </Fragment>
  );
}


export default App;
