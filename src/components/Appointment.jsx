import React from 'react'

const Appointment = ({apointment, deleteAppointment}) => {
  return (
    <div className="apointment">
     <p>Pacient:{apointment.pacient} <span></span></p> 
     <p>Date:{apointment.apDate} <span></span></p> 
     <p>Hour:{apointment.apTime} <span></span></p> 
     <p>Sintoms:{apointment.sintoms} <span></span></p> 
      <button
        className="button delete u-full-width"
        onClick={() => deleteAppointment(apointment.id)}
      >Delete &times;</button>
    </div>
  )
}

export default Appointment
