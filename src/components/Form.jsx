import React, { Fragment, useState } from 'react'
import { v4 as uuidv4 } from 'uuid';
import PropTypes from 'prop-types';

const Form = ({createApointment}) => {

  // Create appointment state
  const [appointment, updateAppointment] = useState({
    pacient: '',
    apDate: '',
    apTime: '',
    sintoms: '',
  });

  const [error, updateError] = useState(false);

  const handleChange = (e) => {
    console.log(e.target.value);
    console.log(e.target.name)
    updateAppointment({
      ...appointment,
      [e.target.name]: e.target.value
    })
  }

  // Extract the values
  const { pacient, apDate, apTime, sintoms } = appointment

  const onSubmit = (e) => {
    e.preventDefault();
    console.log("Sending...");

    // Validation
    if (pacient.trim() === '' || apDate.trim() === '' ||
      apTime.trim() === '' || sintoms.trim() === '') {
      updateError(true);
      return
    } 

    updateError(false);

    // Put an ID
    appointment.id = uuidv4();

    // Create an apoinment
    createApointment(appointment);

    // Clean the form fields
    updateAppointment({
    pacient: '',
    apDate: '',
    apTime: '',
    sintoms: '',
    })
  }
  

  return (
    <Fragment>
      <h2>Create Appointment</h2>
      <form onSubmit={onSubmit}>
        { error ? <p className="alert-error">Snapp! All fields are required!</p> : null}
        <label>Pacient Name</label>
        <input
          type="text"
          name="pacient"
          className="u-full-width"
          placeholder="Pacient Name"
          onChange={handleChange}
          value={pacient}
        />
        <label>Appointmet Date</label>
        <input
          type="date"
          name="apDate"
          className="u-full-width"
          onChange={handleChange}
          value={apDate}
        />
        <label>Time</label>
        <input
          type="time"
          name="apTime"
          className="u-full-width"
          onChange={handleChange}
          value={apTime}
        />
        <label>Sintoms</label>
        <textarea
          name="sintoms"
          className="u-full-width"
          onChange={handleChange}
          value={sintoms}
        />
        <button
          type="submit"
          className="u-full-width button-primary"
        >Add Appointment
        </button>
      </form>
    </Fragment>
  )
}

Form.propTypes = {
  createApointment: PropTypes.func.isRequired
}

export default Form;